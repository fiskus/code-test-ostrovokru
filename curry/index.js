Function.prototype.curry = function(a) {
  var that = this;
  var prevArgs = [].slice.apply(arguments);
  if (prevArgs.length < that.length) {
    return function() {
      var postArgs = [].slice.apply(arguments);
      var args = prevArgs.concat.apply(prevArgs, postArgs);
      return that.curry.apply(that, args);
    };
  } else {
    return that.apply(that, prevArgs);
  }
}


function abc(a, b, c) {
  return a + b + c;
}

function abcdef(a, b, c, d, e, f) {
  return a + b + c + d + e + f;
}

function assert (a, b) {
  if (a !== b) {
    throw new Error();
  }
}
assert(abc.curry('A', 'B', 'C'), 'ABC');
assert(abc.curry('A')('B')('C'), 'ABC');
assert(abc.curry('A', 'B')('C'), 'ABC');
assert(abcdef.curry('A')('B')('C')('D')('E')('F'), 'ABCDEF');
assert(abcdef.curry('A', 'B', 'C')('D', 'E', 'F'), 'ABCDEF');
